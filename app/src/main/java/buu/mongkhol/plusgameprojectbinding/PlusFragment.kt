package buu.mongkhol.plusgameprojectbinding

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import buu.mongkhol.plusgameprojectbinding.databinding.ActivityMainBinding
import buu.mongkhol.plusgameprojectbinding.databinding.FragmentPlusBinding
import kotlinx.android.synthetic.main.fragment_plus.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class PlusFragment : Fragment() {

    private var plusCorrect:Int = 0
    private var plusIncorrect:Int = 0
    private var N1 = Random.nextInt(0, 11)
    private var N2 = Random.nextInt(0, 11)

    private lateinit var binding: FragmentPlusBinding
    // TODO: Rename and change types of parameters

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentPlusBinding>(inflater, R.layout.fragment_plus, container,false)

        binding.txtTruePlus.setTextColor(Color.parseColor("#00ff33"))
        binding.txtTrPlus.setTextColor(Color.parseColor("#00ff33"))
        binding.txtFalsePlus.setTextColor(Color.parseColor("#ff33cc"))
        binding.txtFaPlus.setTextColor(Color.parseColor("#ff33cc"))

        reGame()

        return binding.root
    }

    private fun reGame() {
        N1 = Random.nextInt(0, 11)
        N2 = Random.nextInt(0, 11)
        val number1 = binding.txtPN1
        val number2 = binding.txtPN2

        number1.text = "$N1"
        number2.text = "$N2"
        val answer = N1 + N2
        val answer2 = answer.toString()

        randomAnswer(binding.btn1, binding.btn2, binding.btn3, answer)
        check(answer2)

    }

    private fun randomAnswer(
        btn1: Button,
        btn2: Button,
        btn3: Button,
        answer: Int
    ) {
        val ans: Int = Random.nextInt(0, 3)
        if (ans == 0) {
            btn1.text = (answer + 0).toString()
            btn2.text = (answer + 5).toString()
            btn3.text = (answer + 2).toString()
        }
        if (ans == 1) {
            btn1.text = (answer - 1).toString()
            btn2.text = (answer + 0).toString()
            btn3.text = (answer + 2).toString()
        }
        if (ans == 2) {
            btn1.text = (answer - 2).toString()
            btn2.text = (answer + 1).toString()
            btn3.text = (answer + 0).toString()
        }
    }

    private fun checkButton(btn: Button, answer: String) {
        btn.setOnClickListener {
            if (answer == btn.text) {
                plusCorrect += 1
                txtTrPlus.text = "ถูก : $plusCorrect"
                btn.setBackgroundColor(Color.parseColor("#69FF78"))
            } else {
                plusIncorrect += 1
                txtFaPlus.text = "ผิด : $plusIncorrect"
                btn.setBackgroundColor(Color.parseColor("#FF8848"))
            }
            Handler().postDelayed({
                btn.setBackgroundColor(Color.parseColor("#E4E1E8"))
                reGame()
            },250)
        }
    }

    private fun check(answer: String){
        binding.apply {
            checkButton(btn1, answer)
            checkButton(btn2, answer)
            checkButton(btn3, answer)
        }
    }

}